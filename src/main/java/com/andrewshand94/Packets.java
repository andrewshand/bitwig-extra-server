package com.andrewshand94;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class Packets {

    public static class EQBit {
        double q;
        double freq;
        double amp;
        double type;
        boolean on;
        int index;
    }
    public static class EQPacket {
        ArrayList<EQBit> bands;

        public EQPacket() {

        }
    }

    public static class TrackPacket {

        public TrackPacket() {

        }
        String name;
        String volumeString;
        double volume;
        double pan;
        String color;
        String type;
        int index;
        boolean mute;
        boolean solo;
        boolean monitor;
        boolean autoMonitor;
        boolean arm;
        boolean selected;
    }

    public static class PacketWrap {
        public PacketWrap() {

        }
        String type;
        Object data;
    }

    public static JsonElement makeResourceModifier(JsonElement resource, JsonElement modifier) {
        JsonObject parent = new JsonObject();
        parent.add("resource", resource);
        parent.add("modifier", modifier);
        return parent;
    }
}
