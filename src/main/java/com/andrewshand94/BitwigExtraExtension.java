package com.andrewshand94;
import com.bitwig.extension.controller.api.*;
import com.bitwig.extension.controller.ControllerExtension;
import com.google.gson.*;

import java.io.*;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

class BitwigExtraExtension extends ControllerExtension {
   static BitwigExtraExtension instance;
   private RemoteSocket s;
   Application app;
   Logger logger;
   TaskMan taskMan;
   Arranger arranger;
   Project project;
   Transport transport;
   private RemoteConnection activeConnection;
   private TrackBank bank;
   private Debouncer d;
   final int TRACK_BANK_SIZE = 125;
   final int DEVICE_BANK_SIZE = 50;
   final int PARAMETER_BANK_SIZE = 48;
   final static String SAVE_DATA_PATH = "/Users/andrewshand/Documents/BitwigExtra";
   int[] aviciLevels = new int[TRACK_BANK_SIZE];
   String[] notes = new String[TRACK_BANK_SIZE];
   CursorRemoteControlsPage[] pages = new CursorRemoteControlsPage[DEVICE_BANK_SIZE];
   CursorTrack arrangerTrack;
   DeviceBank arrangeDeviceBank;
   int arrangerTrackPosition = 0;
   private Gson gson;
   boolean debug = false;
   private PopupBrowser currentBrowser;
   private String currentSelectedTrackName = null;

   static BitwigExtraExtension getInstance() {
      return instance;
   }

   protected BitwigExtraExtension(final BitwigExtraExtensionDefinition definition, final ControllerHost host) {
      super(definition, host);
      instance = this;
   }

   private void send(RemoteConnection conn, String str) {
      try {
         this.printDebug("sending: " + str);
         conn.send(("<MESSAGEHEADERFUCKYOU>" + str).getBytes());
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   private void processedReceivedString(String s) {
      printDebug("recieved: " + s);
      JsonObject packet = gson.fromJson(s, JsonObject.class);
      receivePacket(packet);
   }

   StringBuilder waiting = new StringBuilder();

   @Override
   public void init() {

      logger = Logger.getLogger("BitwigExtra");
      printDebug("test");
      try {
         File logFile = new File("/Users/andrewshand/bitwigextra");
         if (!logFile.exists()) {
            logFile.createNewFile();
         }
         FileHandler handler = new FileHandler(logFile.getAbsolutePath(), true);
         logger.addHandler(handler);
         handler.setFormatter(new SimpleFormatter());
      } catch (IOException e) {
         e.printStackTrace();
      }
      d = new Debouncer();
      gson = new Gson();
      taskMan = new TaskMan(this);
      app = this.getHost().createApplication();
      app.projectName().markInterested();

      transport = this.getHost().createTransport();
      transport.getPosition().markInterested();
      transport.isPlaying().markInterested();
      transport.isArrangerRecordEnabled().markInterested();
      transport.isArrangerLoopEnabled().markInterested();
      transport.getInPosition().markInterested();
      transport.getOutPosition().markInterested();
      transport.isMetronomeEnabled().markInterested();
      transport.getTimeSignature().markInterested();
      transport.getTimeSignature().getDenominator().markInterested();
      transport.getTimeSignature().getNumerator().markInterested();
      transport.automationWriteMode().markInterested();
      transport.isArrangerAutomationWriteEnabled().markInterested();

      arranger = this.getHost().createArranger();
      currentBrowser = this.getHost().createPopupBrowser();
      project = this.getHost().getProject();
      for (int x = 0; x < TRACK_BANK_SIZE; x++) {
         notes[x] = "[]";
      }

      s = this.getHost().createRemoteConnection("something", 8888);
      s.setClientConnectCallback(connection -> {
         activeConnection = connection;
         printDebug("hi there!");
         waiting = new StringBuilder();

         activeConnection.setDisconnectCallback(() -> {
            printDebug("closed");
            activeConnection = null;
         });
         activeConnection.setReceiveCallback(data -> {
            printDebug("Received SOETHING");
            String header = "<header>";
            String footer = "<footer>";
            try {
               waiting.append(new String(data, "UTF-8"));
               while (waiting.indexOf(footer) >= 0) {
                  String found = waiting.substring(header.length(), waiting.indexOf(footer));
                  processedReceivedString(found);
                  printDebug(found);
                  waiting = new StringBuilder(waiting.substring(waiting.indexOf(footer) + footer.length()));
               }
            } catch (Exception e) {
               printDebug(e.toString());
               e.printStackTrace();
            }
         });

         sendAllTracks();
//         sendTrackEQ(null, null, null);
      });

      bank = project.getRootTrackGroup().createTrackBank(TRACK_BANK_SIZE, 20, 0, true);
      bank.itemCount().markInterested();

      for (int i = 0; i < bank.getSizeOfBank(); i++) {
         final int i2 = i;
         Track actualTrack = bank.getItemAt(i);
         actualTrack.position().markInterested();
         actualTrack.name().markInterested();
         actualTrack.getVolume().displayedValue().markInterested();
         actualTrack.getVolume().value().markInterested();
         actualTrack.getVolume().displayedValue().addValueObserver(newValue -> {
            sendSingleTrack(actualTrack, activeConnection);
         });
         actualTrack.getArm().markInterested();
         actualTrack.getArm().addValueObserver(newValue -> {
            sendSingleTrack(actualTrack, activeConnection);
         });
         actualTrack.getMonitor().markInterested();
         actualTrack.getMonitor().addValueObserver(newValue -> {
            sendSingleTrack(actualTrack, activeConnection);
         });
         actualTrack.getAutoMonitor().markInterested();
         actualTrack.getPan().value().markInterested();
         actualTrack.color().markInterested();
         actualTrack.exists().markInterested();
         actualTrack.getMute().markInterested();
         actualTrack.getMute().addValueObserver(value -> {
            sendSingleTrack(actualTrack, activeConnection);
         });
         actualTrack.getSolo().markInterested();
         actualTrack.getSolo().addValueObserver(value -> {
            sendSingleTrack(actualTrack, activeConnection);
         });
         actualTrack.trackType().markInterested();
         actualTrack.addVuMeterObserver(128, -1, true, value -> {
            aviciLevels[i2] = value;
         });
         actualTrack.addNoteObserver((noteOn, key, velocity) -> {
            String keyString = String.valueOf(key);
            boolean contains = notes[i2].contains(keyString);
            if (contains && !noteOn) {
               notes[i2] = notes[i2].replaceFirst(keyString + ",?", "").replaceFirst(",]", "]");
            } else if (!contains && noteOn) {
               notes[i2] = notes[i2].length() == 2
                       ? "[" + keyString + "]"
                       : notes[i2].substring(0, notes[i2].length() - 1) + "," + keyString + "]";
            }
         });
         actualTrack.name().addValueObserver((newName) -> {
            sendAllTracks();
         });
      }
      bank.itemCount().addValueObserver(newValue -> sendAllTracks());
      printDebug("Ready");

      // Setup cursor track/devices
      arrangerTrack = this.getHost().createCursorTrack("0", "cursor", 20, 0, true);
      arrangerTrack.name().markInterested();
      arrangerTrack.name().addValueObserver((something) -> {
         String oldSelected = currentSelectedTrackName;
         printDebug("selected track changed");
         if (activeConnection == null) {
            printDebug("no connection, returning");
            return;
         }

         if (oldSelected != null) {
            printDebug("sending old track not selected");
            // let them know the old track is deselected
            JsonObject resource = new JsonObject();
            JsonObject trackResource = new JsonObject();
            trackResource.addProperty("name", oldSelected);
            resource.add("track", trackResource);

            JsonObject modifier = new JsonObject();
            modifier.addProperty("selected", false);

            send(activeConnection, gson.toJson(Packets.makeResourceModifier(resource, modifier)));
         }

         printDebug("sending new track selected");
         // set the new track as selected
         JsonObject resource = new JsonObject();
         JsonObject trackResource = new JsonObject();
         trackResource.addProperty("name", something);
         resource.add("track", trackResource);

         JsonObject modifier = new JsonObject();
         modifier.addProperty("selected", true);

         send(activeConnection, gson.toJson(Packets.makeResourceModifier(resource, modifier)));
         currentSelectedTrackName = something;
      });
      arrangerTrack.position().markInterested();
      arrangerTrack.position().addValueObserver((something) -> {
         printDebug("new track! " + something);
         arrangerTrackPosition = something;
//         this.sendTrackEQ(null, null, null);
      });
      arrangeDeviceBank = arrangerTrack.createDeviceBank(DEVICE_BANK_SIZE);
      for (int i = 0; i < DEVICE_BANK_SIZE; i++) {
         Device device = arrangeDeviceBank.getItemAt(i);
         device.exists().markInterested();
//         device.addDirectParameterIdObserver(new StringArrayValueChangedCallback() {
//            @Override
//            public void valueChanged(String[] newValue) {
//               for (int i = 0; i < newValue.length; i++) {
//                  println(newValue[i]);
//               }
//            }
//         });
         device.name().markInterested();
         device.isEnabled().markInterested();

         pages[i] = device.createCursorRemoteControlsPage("something" + i, PARAMETER_BANK_SIZE, null);
         pages[i].selectedPageIndex().markInterested();

         for (int par = 0; par < PARAMETER_BANK_SIZE; par++) {
            pages[i].getParameter(par).exists().markInterested();
            pages[i].getParameter(par).value().markInterested();
            pages[i].getParameter(par).name().markInterested();
         }
      }
   }

   private void receivePacket(JsonObject packet) {
      this.printDebug("Received packet");
      String resourceAsString = packet.get("resource").getAsString();
      JsonElement modifier = packet.get("modifier");

      if (resourceAsString.equals("recording")) {
         transport.isArrangerRecordEnabled().set(modifier.getAsBoolean());
      }
      if (resourceAsString.equals("transport.playing")) {
         transport.isPlaying().set(modifier.getAsBoolean());
      }
      if (resourceAsString.equals("transport.looping")) {
         transport.isArrangerLoopEnabled().set(modifier.getAsBoolean());
      }
      if (resourceAsString.equals("metronome")) {
         transport.isMetronomeEnabled().set(modifier.getAsBoolean());
      }
      if (resourceAsString.equals("undo")) {
         app.undo();
      }
      if (resourceAsString.equals("redo")) {
         app.redo();
      }
      if (resourceAsString.equals("transport.rewind")) {
         transport.rewind();
      }
      if (resourceAsString.equals("transport.fastForward")) {
         transport.fastForward();
      }
      if (resourceAsString.startsWith("tracks/")) {
         this.receiveTrackPacket(packet);
      }
   }


   private void sendLoadedFile(String fileId, String content) {
      if (activeConnection == null) {
         return;
      }

      JsonObject obj = new JsonObject();
      JsonObject resource = new JsonObject();
      resource.addProperty("data", fileId);
      obj.add("resource", resource);
      obj.addProperty("modifier", content);

      send(activeConnection, gson.toJson(obj));
   }

   private void receiveTrackPacket(JsonObject packet) {
      printDebug("received track packet");
      String resource = packet.get("resource").getAsString();
      JsonObject modifier = packet.get("modifier").getAsJsonObject();

      List<Track> tracks = new ArrayList<>();

//      if (trackObj.has("index")) {
//         if (trackObj.get("index").isJsonObject()) {
//            JsonObject index = trackObj.get("index").getAsJsonObject();
//            if (index.has("ne")) {
//               int neIndex = trackObj.get("index").getAsJsonObject().get("ne").getAsInt();
//               // all tracks _except_ an index
//               for (int i = 0; i < bank.getSizeOfBank(); i++) {
//                  if (bank.getItemAt(i).exists().get() && i != neIndex) {
//                     Track track = bank.getItemAt(i);
//                     tracks.add(track);
//                  }
//               }
//               sendAllTracks();
//            }
//            else if (index.has("in")) {
//               JsonArray inArray = trackObj.get("index").getAsJsonObject().get("ne").getAsJsonArray();
//               HashSet<Integer> trackIds = new HashSet<>();
//               for (JsonElement o : inArray) {
//                  trackIds.add(o.getAsInt());
//               }
//               // all tracks _except_ an index
//               for (int i = 0; i < bank.getSizeOfBank(); i++) {
//                  if (bank.getItemAt(i).exists().get() && trackIds.contains(i)) {
//                     Track track = bank.getItemAt(i);
//                     tracks.add(track);
//                  }
//               }
//               sendAllTracks();
//            }
//         }
//         else {
//            Track track = bank.getItemAt(trackObj.get("index").getAsInt());
//            tracks.add(track);
//            sendSingleTrack(track, connection); // this will happen after the update
//         }
//
//      }
// if (trackObj.has("name")) {
         String name = resource.replace("tracks/", "");
         for (int i = 0; i < bank.getSizeOfBank(); i++) {
            if (bank.getItemAt(i).exists().get() && bank.getItemAt(i).name().get().equals(name)) {
               Track track = bank.getItemAt(i);
               tracks.add(track);
            }
         }
//      }
//      else {
//         // no selector, all tracks!
//         for (int i = 0; i < TRACK_BANK_SIZE; i++) {
//            if (bank.getItemAt(i).exists().get()) {
//               Track track = bank.getItemAt(i);
//               tracks.add(track);
//            }
//         }
//         sendAllTracks();
//      }

      printDebug("editing affected tracks");
      for (Track track : tracks) {
         if (modifier.has("volume")) {
            track.getVolume().set(modifier.get("volume").getAsFloat());
         }

         if (modifier.has("solo")) {
            track.getSolo().set(modifier.get("solo").getAsBoolean());
         }

         if (modifier.has("name")) {
            printDebug("setting name to " + modifier.get("name"));
            track.name().set(modifier.get("name").getAsString());
         }

         if (modifier.has("mute")) {
            track.getMute().set(modifier.get("mute").getAsBoolean());
         }

         if (modifier.has("selected") && modifier.get("selected").getAsBoolean()) {
            track.selectInEditor();
            track.makeVisibleInArranger();
         }

         if (modifier.has("arm")) {
            track.getArm().set(modifier.get("arm").getAsBoolean());
         }

         if (modifier.has("autoMonitor")) {
            track.getAutoMonitor().set(modifier.get("autoMonitor").getAsBoolean());
         }

         if (modifier.has("monitor")) {
            track.getMonitor().set(modifier.get("monitor").getAsBoolean());
         }

         if (modifier.has("eq")) {
            processEQPacket(track, gson.fromJson(modifier.get("eq"), Packets.EQPacket.class));
         }
      }
      printDebug("finished editing affected tracks");
   }

   private void runSoon(Runnable task, String id) {
      taskMan.schedule(id, task);
   }

   @Override
   public void exit()
   {
      // TODO: Perform any cleanup once the driver exits
      // For now just show a popup notification for verification that it is no longer running.
      getHost().showPopupNotification("Bitwig Extra Exited");
   }

   JsonObject makeTrackResource(Track track) {
      JsonObject resource = new JsonObject();
      JsonObject trackResource = new JsonObject();
      trackResource.addProperty("index", track.position().get());
      resource.add("track", trackResource);
      return resource;
   }

   private void sendSingleTrack(Track track, RemoteConnection connection) {
      runSoon(() -> {
         JsonObject resource = new JsonObject();
         JsonObject trackResource = new JsonObject();
         trackResource.addProperty("name", track.name().get());
         resource.add("track", trackResource);

         JsonObject modifier = new JsonObject();
         modifier.addProperty("volumeString", track.getVolume().displayedValue().get());
         modifier.addProperty("volume", track.getVolume().get());
         modifier.addProperty("mute", track.getMute().get());
         modifier.addProperty("solo", track.getSolo().get());
         modifier.addProperty("name", track.name().get());
         modifier.addProperty("autoMonitor", track.getAutoMonitor().get());
         modifier.addProperty("arm", track.getArm().get());
         modifier.addProperty("monitor", track.getMonitor().get());
         modifier.addProperty("selected", track.name().get().equals(currentSelectedTrackName));

         send(connection, gson.toJson(Packets.makeResourceModifier(resource, modifier)));
      }, String.valueOf(track.name().get())); // todo better id
   }

   private void printDebug(String str) {
   if (debug) {
      getHost().println(str);
//         logger.log(Level.INFO, str);
      //println(str);
   }

   }

   private void processEQPacket(Track track, Packets.EQPacket packet) {
      // this should only ever be for the currently selected track
      if (!track.name().get().equals(arrangerTrack.name().get())) {
         printDebug("only editing the currently selected track's EQ is currently supported");
         printDebug("current arranger track is " + arrangerTrackPosition);
         return;
      }

      int totalBands = 0;
      printDebug("start eq packet");
      for (int i = 0; i < DEVICE_BANK_SIZE && totalBands < packet.bands.size(); i++) {
         Device thisDevice = arrangeDeviceBank.getDevice(i);
         if (!thisDevice.exists().get() || !thisDevice.isEnabled().get()) {
            return;
         }
         if (thisDevice.name().get().equalsIgnoreCase("EQ-5")) {
            printDebug("start eq packet device");
            for (int b = 0; b < 5 && totalBands < packet.bands.size(); b++) {
               Packets.EQBit band = packet.bands.get(totalBands);
               String id = String.valueOf(b + 1);
               thisDevice.setDirectParameterValueNormalized("CONTENTS/GAIN" + id, band.amp * 255, 255);
               thisDevice.setDirectParameterValueNormalized("CONTENTS/FREQ" + id, band.freq * 255, 255);
               thisDevice.setDirectParameterValueNormalized("CONTENTS/Q" + id, band.q * 255, 255);
               thisDevice.setDirectParameterValueNormalized("CONTENTS/ENABLE" + id, band.on ? 255 : 0, 255);
               thisDevice.setDirectParameterValueNormalized("CONTENTS/TYPE" + id, band.type * 255, 255);
               totalBands++;
            }
            printDebug("end eq packet device");
         }
      }
      printDebug("end eq packet");
   }

   private void sendTrackEQ(Integer asyncPage, Packets.EQPacket packetSoFar, String trackName) {
      if (asyncPage == null) {
         asyncPage = 0;
      }
      if (packetSoFar == null) {
         packetSoFar = new Packets.EQPacket();
         packetSoFar.bands = new ArrayList<>();
      }
      if (trackName == null) {
         trackName = arrangerTrack.name().get();
      }
      else if (!arrangerTrack.name().get().equals(trackName)) {
         printDebug("halting eq packet because current track changed");
         return;
      }

      if (activeConnection == null) {
         return;
      }

      printDebug("Sending round " + asyncPage + " of track: " + trackName);
      final int thePage = asyncPage;
      int totalBands = 0;
      for (int i = 0; i < DEVICE_BANK_SIZE; i++) {
         Device thisDevice = arrangeDeviceBank.getDevice(i);
         CursorRemoteControlsPage page = pages[i];

         if (thisDevice.exists().get() && thisDevice.isEnabled().get() && thisDevice.name().get().equalsIgnoreCase("EQ-5")) {
            printDebug("Device " + i + " is EQ");
            for (int b = 0; b < 5; b++) {
               int bandIndex = totalBands + b;
               if (packetSoFar.bands.size() <= bandIndex) {
                  packetSoFar.bands.add(new Packets.EQBit());
               }
               Packets.EQBit band = packetSoFar.bands.get(bandIndex);
               if (thePage == 0) {
                  band.amp = page.getParameter(b).get();
               }
               if (thePage == 1) {
                  band.freq = page.getParameter(b).get();
               }
               if (thePage == 2) {
                  band.q = page.getParameter(b).get();
               }
               if (thePage == 3) {
                  band.type = page.getParameter(b).get();
               }
               if (thePage == 4) {
                  band.on = page.getParameter(b).get() == 1;
               }
               band.index = bandIndex;
            }

            // to prepare for the next call of this function
            if (thePage == 4) {
               page.selectedPageIndex().set(0);
            }
            else {
               page.selectedPageIndex().set(thePage + 1);
            }
            totalBands += 5;
         }
      }

      if (thePage < 4) {
         final Packets.EQPacket packet = packetSoFar;
         final String nameNow = trackName;
         this.getHost().scheduleTask(() -> {
            this.sendTrackEQ(thePage + 1, packet, nameNow);
         }, 50);
      }
      else {
         JsonObject modifier = new JsonObject();
         modifier.add("eq", gson.toJsonTree(packetSoFar, Packets.EQPacket.class));

         JsonObject resource = new JsonObject();
         JsonObject trackResource = new JsonObject();
         trackResource.addProperty("name", trackName);
         resource.add("track", trackResource);

         JsonElement wholePacket = Packets.makeResourceModifier(resource, modifier);
         String json = gson.toJson(wholePacket);

         send(activeConnection, json);
      }
   }

   private void sendAllTracks() {
      printDebug("scheduling send of all tracks");
      runSoon(() -> {
         if (activeConnection == null) {
            return;
         }
         printDebug("going to send all tracks");
         HashSet<String> namesSoFar = new HashSet<>();
         List<Packets.TrackPacket> tracks = new ArrayList<>();
         for (int i = 0; i < TRACK_BANK_SIZE; i++) {
            printDebug("track " + i);
            Track actualTrack = bank.getItemAt(i);
            if (actualTrack == null || !actualTrack.exists().get()) {
               break;
            }

            Packets.TrackPacket t = new Packets.TrackPacket();
            String name = actualTrack.name().get();
            if (namesSoFar.contains(name)) {
               getHost().showPopupNotification("Warning, track \"" + name + "\"" + " has duplicate name");
               continue;
            }

            t.name = name;
            namesSoFar.add(name);
            t.type = actualTrack.trackType().get();
            t.volume = actualTrack.getVolume().value().get();
            t.volumeString = actualTrack.getVolume().displayedValue().get();
            t.pan = actualTrack.getPan().value().get();
            t.color = Color.toString(actualTrack.color());
            t.mute = actualTrack.getMute().get();
            t.solo = actualTrack.getSolo().get();
            t.index = i;
            t.arm = actualTrack.getArm().get();
            t.autoMonitor = actualTrack.getAutoMonitor().get();
            t.monitor = actualTrack.getMonitor().get();
            t.selected = actualTrack.name().get().equals(currentSelectedTrackName);
            tracks.add(t);
         }

         printDebug("finishing iterating");
         Packets.PacketWrap wrap = new Packets.PacketWrap();
         wrap.type = "tracks";
         wrap.data = tracks;
         send(activeConnection, gson.toJson(wrap));
//         sendTrackEQ(null, null, null);
         printDebug("all tracks sent!");
      }, "allTracks");
   }


   private void sendTransportPacket() {
      JsonObject packet = new JsonObject();
      JsonObject modifier = new JsonObject();
      modifier.addProperty("position", this.transport.getPosition().get());
      modifier.addProperty("timeSignature", this.transport.getTimeSignature().get());
      modifier.addProperty("playing", this.transport.isPlaying().get());
      modifier.addProperty("looping", this.transport.isArrangerLoopEnabled().get());
      modifier.addProperty("recording", this.transport.isArrangerRecordEnabled().get());
      modifier.addProperty("in", this.transport.getInPosition().get());
      modifier.addProperty("out", this.transport.getOutPosition().get());
      modifier.addProperty("timeSignature", this.transport.getTimeSignature().get());
      modifier.addProperty("metronome", this.transport.isMetronomeEnabled().get());
      packet.add("modifier", modifier);
      packet.addProperty("resource", "transport");
      send(activeConnection, gson.toJson(packet));
   }

   private void sendStatePacket() {
      // send state update
      JsonObject statePacket = new JsonObject();
      JsonObject stateModifier = new JsonObject();
      stateModifier.addProperty("browsing", this.currentBrowser != null);
      statePacket.add("modifier", stateModifier);
      statePacket.addProperty("resource", "state");
      send(activeConnection, gson.toJson(statePacket));
   }

   private void sendLevels() {
      send(activeConnection, "{\"type\":\"notes\",\"data\":" + Arrays.toString(notes) + "}");
      send(activeConnection, "{\"type\":\"levels\",\"data\":" + Arrays.toString(aviciLevels) + "}");
   }

   @Override
   public void flush()
   {
      if (activeConnection == null) {
         return;
      }

      sendLevels();
      sendTransportPacket();
      sendStatePacket();
   }
}
