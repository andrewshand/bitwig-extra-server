package com.andrewshand94;
import java.util.UUID;

import com.bitwig.extension.api.PlatformType;
import com.bitwig.extension.controller.AutoDetectionMidiPortNamesList;
import com.bitwig.extension.controller.ControllerExtensionDefinition;
import com.bitwig.extension.controller.api.ControllerHost;

public class BitwigExtraExtensionDefinition extends ControllerExtensionDefinition
{
   private static final UUID DRIVER_ID = UUID.fromString("cc874172-1554-4a53-9f37-57b13ab8b0af");
   
   public BitwigExtraExtensionDefinition()
   {
   }

   @Override
   public String getName()
   {
      return "Bitwig Extra";
   }
   
   @Override
   public String getAuthor()
   {
      return "andrewshand94";
   }

   @Override
   public String getVersion()
   {
      return "0.1";
   }

   @Override
   public UUID getId()
   {
      return DRIVER_ID;
   }
   
   @Override
   public String getHardwareVendor()
   {
      return "andrewshand94";
   }
   
   @Override
   public String getHardwareModel()
   {
      return "Bitwig Extra";
   }

   @Override
   public int getRequiredAPIVersion()
   {
      return 3;
   }

   @Override
   public int getNumMidiInPorts()
   {
      return 0;
   }

   @Override
   public int getNumMidiOutPorts()
   {
      return 0;
   }

   @Override
   public void listAutoDetectionMidiPortNames(final AutoDetectionMidiPortNamesList list, final PlatformType platformType)
   {
   }

   @Override
   public BitwigExtraExtension createInstance(final ControllerHost host)
   {
      return new BitwigExtraExtension(this, host);
   }
}
