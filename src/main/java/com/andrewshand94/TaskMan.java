package com.andrewshand94;

import java.util.HashMap;
import java.util.Map;

public class TaskMan {
    BitwigExtraExtension extension;
    HashMap<String, Runnable> tasksByType;

    public TaskMan(BitwigExtraExtension extension) {
        this.extension = extension;
        this.tasksByType = new HashMap<>();
        scheduleNextRun();
    }

    public void schedule(String id, Runnable r) {
        if (tasksByType.containsKey(id)) {
            tasksByType.remove(id);
        }
        tasksByType.put(id, r);
    }

    public void scheduleNextRun() {
        extension.getHost().scheduleTask(() -> {
            for (Runnable task : tasksByType.values()) {
                try {
                    task.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            tasksByType = new HashMap<>();
            scheduleNextRun();
        }, 250);
    }
}
