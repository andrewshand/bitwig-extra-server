package com.andrewshand94;

import com.bitwig.extension.controller.api.SettableColorValue;

public class Color {
    public static String toString(SettableColorValue c) {
        String r = Integer.toHexString(Math.round(c.red() * 255f));
        if (r.length() == 1) {
            r = "0" + r;
        }
        String g = Integer.toHexString(Math.round(c.green() * 255f));
        if (g.length() == 1) {
            g = "0" + g;
        }
        String b = Integer.toHexString(Math.round(c.blue() * 255f));
        if (b.length() == 1) {
            b = "0" + b;
        }
        return "#" + r + g + b;
    }
}
