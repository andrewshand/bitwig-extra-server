# bitwig-extra-server

The server component of https://github.com/andrewshand/bitwig-extra-client

You need to run both to do anything useful

# Building

Not massively familiar with java build pipelines, but this uses Maven, I know that much. IntelliJ will hold your hand. You want the pipeline called install, which will create a .bwextension file.

Or just download the latest release.
